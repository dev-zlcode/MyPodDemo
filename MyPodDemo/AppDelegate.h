//
//  AppDelegate.h
//  MyPodDemo
//
//  Created by zhanglei on 2019/1/18.
//  Copyright © 2019 zlcode. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

